import os
from langchain.llms import OpenAI
from langchain import FewShotPromptTemplate, PromptTemplate
from langchain.chains import LLMChain

os.environ["OPENAI_API_KEY"]

llm = OpenAI(model_name="text-davinci-003", temperature=0.9, max_tokens= 256)

examples = [
    {"input": "Apple is a continent", "output": "Apple is not a continent. It is a fruit. Continents are large land masses on Earth, such as Asia, Africa, North America, etc"},
    {"input": "10 is smaller than 0", "output": "10 is greater than 0"},
    {"input": "There was a fish who could swim", "output": "There was a fish who could swim"},
]

example_formatter_template = """
Input: {input}
Output: {output}\n
"""
example_prompt = PromptTemplate(
    input_variables=["input", "output"],
    template=example_formatter_template,
)

few_shot_prompt = FewShotPromptTemplate(
    examples=examples,
    example_prompt=example_prompt,
    prefix=("you are a sentence corrector. your task is to modify and correct the sentences given as input, modify only that sentence which does not make sense generally or is factually wrong. After processing the input return it's corrected version only if correction is required."
            "See some following examples:"),
    suffix="Input: {input}\nOutput:",
    input_variables=["input"],
    example_separator="\n\n",
)

chain = LLMChain(llm=llm, prompt=few_shot_prompt)

async def check_sentence_by_chatgpt(input_sentence):
    few_shot_prompt.format(input=input_sentence)
    res = await chain.arun(input_sentence)
    return res.strip()
