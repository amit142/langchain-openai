from typing import Annotated
import aiofiles

from fastapi import FastAPI, Form
from fastapi.responses import HTMLResponse
from openai_api import check_sentence_by_chatgpt

app = FastAPI()

@app.get("/", response_class=HTMLResponse)
async def index():
    async with aiofiles.open("index.html", mode="r") as f:
        return await f.read()


@app.post(path="/sentence")
async def check_sentence(inputsentence: Annotated[str, Form()]):
    return await check_sentence_by_chatgpt(inputsentence)
